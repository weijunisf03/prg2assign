﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HotelApp
{



    /// <summary>
    ///
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<HotelRoom> hotelList = new List<HotelRoom>();
        List<HotelRoom> availrooms = new List<HotelRoom>();
        List<HotelRoom> selectedroom = new List<HotelRoom>();
        List<Guest> guestList = new List<Guest>();



        public void InitData()
        {
            HotelRoom s1 = new StandardRoom("Standard", "101", "Single", 90.0, true, 1);
            HotelRoom s2 = new StandardRoom("Standard", "102", "Single", 90.0, true, 1);
            HotelRoom s3 = new StandardRoom("Standard", "201", "Twin", 110.0, true, 2);
            HotelRoom s4 = new StandardRoom("Standard", "202", "Twin", 110.0, true, 2);
            HotelRoom s5 = new StandardRoom("Standard", "203", "Twin", 110.0, true, 2);
            HotelRoom s6 = new StandardRoom("Standard", "301", "Triple", 120.0, true, 3);
            HotelRoom s7 = new StandardRoom("Standard", "302", "Triple", 120.0, true, 3);
            HotelRoom d1 = new DeluxeRoom("Deluxe", "204", "Twin", 140.0, true, 2);
            HotelRoom d2 = new DeluxeRoom("Deluxe", "205", "Twin", 140.0, true, 2);
            HotelRoom d3 = new DeluxeRoom("Deluxe", "303", "Triple", 210.0, true, 3);
            HotelRoom d4 = new DeluxeRoom("Deluxe", "304", "Triple", 210.0, true, 3);
            hotelList.Add(s1);
            hotelList.Add(s2);
            hotelList.Add(s3);
            hotelList.Add(s4);
            hotelList.Add(s5);
            hotelList.Add(s6);
            hotelList.Add(s7);
            hotelList.Add(d1);
            hotelList.Add(d2);
            hotelList.Add(d3);
            hotelList.Add(d4);
            //Amelia
            Stay g1stay = new Stay(new DateTime(2019, 1, 26), new DateTime(2019, 2, 2));
            StandardRoom sr1 = (StandardRoom)s1;
            sr1.RequireBreakfast = true;
            sr1.RequireWifi = true;
            HotelRoom hr1 = (StandardRoom)sr1;
            Guest guest1 = new Guest("Amelia", "S1234567A", g1stay, new Membership("Gold", 280), true);
            guest1.HotelStay.AddRoom(hr1);
            hr1.IsAvail = false;
            guestList.Add(guest1);
            //Bob
            Stay g2stay = new Stay(new DateTime(2019, 1, 25), new DateTime(2019, 1, 31));
            StandardRoom sr7 = (StandardRoom)s7;
            sr7.RequireBreakfast = true;
            HotelRoom hr7 = (StandardRoom)sr7;
            Guest guest2 = new Guest("Bob", "G1234567A", g2stay, new Membership("Ordinary", 0), true);
            guest2.HotelStay.AddRoom(hr7);
            hr7.IsAvail = false;
            guestList.Add(guest2);
            //Cody
            Stay g3stay = new Stay(new DateTime(2019, 2, 1), new DateTime(2019, 2, 6));
            StandardRoom sr4 = (StandardRoom)s4;
            sr4.RequireBreakfast = true;
            HotelRoom hr4 = (StandardRoom)sr4;
            Guest guest3 = new Guest("Cody", "G2345678A", g3stay, new Membership("Silver", 190), true);
            guest3.HotelStay.AddRoom(hr4);
            hr4.IsAvail = false;
            guestList.Add(guest3);
            //Edda
            Stay g4stay = new Stay(new DateTime(2019, 1, 28), new DateTime(2019, 2, 10));
            DeluxeRoom dr3 = (DeluxeRoom)d3;
            dr3.AdditionalBed = true;
            HotelRoom dhr3 = (DeluxeRoom)dr3;
            Guest guest4 = new Guest("Edda", "S3456789A", g4stay, new Membership("Gold", 10), true);
            guest4.HotelStay.AddRoom(dhr3);
            dhr3.IsAvail = false;
            guestList.Add(guest4);




            //StandardRoom sr2 = new StandardRoom
            //HotelRoom



        }
        public MainPage()
        {
            InitData();
            this.InitializeComponent();
            memberBlk.Visibility = Visibility.Collapsed;
            pointsBlk.Visibility = Visibility.Collapsed;
            pointsTxt.Visibility = Visibility.Collapsed;
            redeemBtn.Visibility = Visibility.Collapsed;
            invoiceBlk.Visibility = Visibility.Collapsed;
            invoicedisplay.Visibility = Visibility.Collapsed;
            selectrmBlk.Visibility = Visibility.Collapsed;
            selectrmLv.Visibility = Visibility.Collapsed;
            chkinBtn.Visibility = Visibility.Collapsed;
            chkoutBtn.Visibility = Visibility.Collapsed;
            statusBlk.Visibility = Visibility.Collapsed;
            removermBtn.Visibility = Visibility.Collapsed;
            availrmBlk.Visibility = Visibility.Collapsed;
            availrmLv.Visibility = Visibility.Collapsed;
            bfast.Visibility = Visibility.Collapsed;
            wifi.Visibility = Visibility.Collapsed;
            bed.Visibility = Visibility.Collapsed;
            addrmBtn.Visibility = Visibility.Collapsed;
            leaderboard.Visibility = Visibility.Collapsed;
        }


        private void ChkrmBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            availrmBlk.Visibility = Visibility.Visible;
            availrmLv.Visibility = Visibility.Visible;
            bfast.Visibility = Visibility.Visible;
            wifi.Visibility = Visibility.Visible;
            bed.Visibility = Visibility.Visible;
            addrmBtn.Visibility = Visibility.Visible;
            availrooms.Clear();

            string adult = adultnoTxt.Text;
            string child = childrennoTxt.Text;

            if (adult == "" || child == "")
            {
                msg.Text = "Please enter both the number of children and adult.";
            }
            else if (adult.Contains("ABCDEFJIHAEHJQH EHQHUIEabcdeffhiashdajihd") || child.Contains("ABCDEFJIHAEHJQH EHQHUIEabcdeffhiashdajihd"))
            {
                msg.Text = "Please enter a number instead";
            }
            else
            {
                foreach (HotelRoom r in hotelList)
                {
                    if (r.IsAvail)
                    {
                        availrooms.Add(r);

                    }
                }
                availrmLv.ItemsSource = availrooms;
            }
        }

        private void AddrmBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            leaderboard.Visibility = Visibility.Collapsed;
            removermBtn.Visibility = Visibility.Visible;
            selectrmBlk.Visibility = Visibility.Visible;
            selectrmLv.Visibility = Visibility.Visible;
            chkinBtn.Visibility = Visibility.Visible;
            chkoutBtn.Visibility = Visibility.Visible;
            HotelRoom chosen = (HotelRoom)availrmLv.SelectedItem;

            if (chosen is StandardRoom)
            {
                StandardRoom s = (StandardRoom)chosen;
                if (wifi.IsChecked == true)
                {
                    s.RequireWifi = true;
                }
                if (bfast.IsChecked == true)
                {
                    s.RequireBreakfast = true;
                }
                //     s.CalculateCharges();
            }
            else if (chosen is DeluxeRoom)
            {
                DeluxeRoom d = (DeluxeRoom)chosen;
                if (bed.IsChecked == true)
                {
                    d.AdditionalBed = true;
                }
                //    d.CalculateCharges();
            }
            foreach (HotelRoom r in hotelList)
            {
                if (r == chosen)
                {
                    chosen.IsAvail = false;
                }
            }
            availrooms.Remove(chosen);
            selectedroom.Add(chosen);
            availrmLv.ItemsSource = null;
            selectrmLv.ItemsSource = null;
            availrmLv.ItemsSource = availrooms;
            selectrmLv.ItemsSource = selectedroom;
        }

        private void ExtendBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            string searchname = guestTxt.Text;
            string searchpassno = ppTxt.Text;
            foreach (Guest g in guestList)
            {
                if ((g.Name == searchname || g.PpNumber == searchpassno) && g.IsCheckedIn == true)
                {
                    g.HotelStay.Extend();
                    msg.Text = "Hotel Stay extended to :" + g.HotelStay.CheckOutDate;
                    
                }
            }
        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            leaderboard.Visibility = Visibility.Collapsed;
            removermBtn.Visibility = Visibility.Collapsed;
            bfast.Visibility = Visibility.Collapsed;
            wifi.Visibility = Visibility.Collapsed;
            bed.Visibility = Visibility.Collapsed;
            addrmBtn.Visibility = Visibility.Collapsed;
            selectrmBlk.Visibility = Visibility.Collapsed;
            selectrmLv.Visibility = Visibility.Collapsed;
            chkinBtn.Visibility = Visibility.Collapsed;
            redeemBtn.Visibility = Visibility.Visible;
            showpts.Visibility = Visibility.Visible;
            showstat.Visibility = Visibility.Visible;
            memberBlk.Visibility = Visibility.Visible;
            pointsBlk.Visibility = Visibility.Visible;
            pointsTxt.Visibility = Visibility.Visible;
            availrmBlk.Visibility = Visibility.Visible;
            availrmLv.Visibility = Visibility.Visible;
            invoiceBlk.Visibility = Visibility.Visible;
            invoicedisplay.Visibility = Visibility.Visible;
            chkoutBtn.Visibility = Visibility.Visible;
            availrmLv.ItemsSource = null;
            string searchname = guestTxt.Text;
            string searchpassno = ppTxt.Text;
            foreach (Guest g in guestList)
            {
                bool bfastcount = false;
                bool wificount = false;
                bool bedcount = false;

                if ((g.Name == searchname || g.PpNumber == searchpassno) && g.IsCheckedIn == true)
                {
                    double total = 0;
                    invoicedisplay.Text = "";
                    availrmLv.ItemsSource = g.HotelStay.Roomlist;
                    int datediff = (g.HotelStay.CheckOutDate - g.HotelStay.CheckInDate).Days;
                    foreach (HotelRoom r in g.HotelStay.Roomlist)
                    {
                        double subtotal = r.CalculateCharges() * datediff;
                        if (r is StandardRoom)
                        {
                            StandardRoom q = (StandardRoom)r;
                            if (q.RequireBreakfast)
                            {
                                bfastcount = true;
                            }
                            if (q.RequireWifi)
                            {
                                wificount = true;
                            }
                            total += subtotal;
                            invoicedisplay.Text += "Room Type:" + r.RoomType + "\nRoom No.:" + r.RoomNumber + "\nNo.of Nights:" + datediff + "\nWifi:" + wificount + "\nBreakfast:" + bfastcount + "\nSubtotal:" + subtotal + "\n\n";
                        }
                        else if (r is DeluxeRoom)
                        {
                            DeluxeRoom z = (DeluxeRoom)r;
                            if (z.AdditionalBed)
                            {
                                bedcount = true;
                            }
                            total += subtotal;
                            invoicedisplay.Text += "Room Type:" + r.RoomType + "\nRoom No.:" + r.RoomNumber + "\nNo.of Nights:" + datediff + "\nAdditional Beds:" + bedcount + "\nSubtotal:" + subtotal + "\n\n";
                        }
                    }

                    showpts.Text = Convert.ToString(g.Membership.Points);
                    showstat.Text = g.Membership.DetermineStat(g.Membership.Points);
                    invoicedisplay.Text += "\n\n\nTotal:" + total + "\n\n\n";
                }
            }
        }

        private void SelectrmLv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void AvailrmLv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bed.IsChecked = false;
            wifi.IsChecked = false;
            bfast.IsChecked = false;
            bed.IsEnabled = true;
            wifi.IsEnabled = true;
            bfast.IsEnabled = true;
            HotelRoom chosen = (HotelRoom)availrmLv.SelectedItem;
            if (chosen is StandardRoom)
            {
                bed.IsEnabled = false;
            }
            else
            {
                wifi.IsEnabled = false;
                bfast.IsEnabled = false;
            }
        }

        private void AdultnoTxt_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private int CreatePerson(string g)
        {
            if (guestList.Count == 0)
            {
                return 1;
            }
            else
            {
                int i = 1;
                foreach (Guest z in guestList)
                {
                    if (z.Name == g || z.PpNumber == g)
                    {
                        i = 0;
                        break;
                    }
                }
                return i;
            }
        }
        private int MaxCap()
        {
            int people = 0;
            foreach(HotelRoom r in selectedroom)
            {
                people += r.NoOfOccupants;
            }
            return people;
        }
        private void ChkinBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            string name = guestTxt.Text;
            string passno = ppTxt.Text;
            int adult = Convert.ToInt32(adultnoTxt.Text);
            int child = Convert.ToInt32(childrennoTxt.Text);
            int totalppl = (adult*2) + child;

            DateTime ci = checkInDate.Date.Value.DateTime;
            DateTime co = checkOutDate.Date.Value.DateTime;

            Stay stay1 = new Stay(checkInDate.Date.Value.DateTime, checkOutDate.Date.Value.DateTime);

            Membership m1 = new Membership("Ordinary", 0);
            if (ci > co)
            {
                msg.Text = "Please ensure your checkout date is later than your check in date!";
            }
            else
            {
                if (name == "" || passno == "")
                {
                    msg.Text = "Please fill in both your credentials";
                }
                else
                {

                    if (CreatePerson(name) == 1 || CreatePerson(passno) == 1)//if need to create a guest
                    {
                        if (totalppl > MaxCap() * 2)
                        {
                            msg.Text = "Please Add a Bed or Room";

                        }
                        else
                        {
                            Guest g1 = new Guest(name, passno, stay1, m1, true);
                            foreach (HotelRoom r in selectedroom)
                            {
                                g1.HotelStay.AddRoom(r);


                            }
                            guestList.Add(g1);
                            selectedroom.Clear();
                            msg.Text = "New guest created";

                        }
                    }
                    else
                    {
                        foreach (Guest g in guestList)
                        {
                            if (g.Name == name || g.PpNumber == passno)
                            {
                                if (totalppl > MaxCap() * 2)
                                {
                                    msg.Text = "Please add a Bed or Room";
                                    break;
                                }
                                else
                                {
                                    g.HotelStay.CheckInDate = ci;
                                    g.HotelStay.CheckOutDate = co;
                                    foreach (HotelRoom r in selectedroom)
                                    {
                                        g.HotelStay.AddRoom(r);



                                    }

                                    selectedroom.Clear();
                                    
                                }
                                g.HotelStay.CheckInDate = ci;
                                g.HotelStay.CheckOutDate = co;


                            }

                        }
                    }

                }
            }



            
            selectrmLv.ItemsSource = null;
            availrmLv.ItemsSource = null;
            selectrmLv.ItemsSource = selectedroom;
            availrmLv.ItemsSource = availrooms;


        }

        private void ChkoutBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            chkoutBtn.Visibility = Visibility.Collapsed;
            invoiceBlk.Visibility = Visibility.Collapsed;
            invoicedisplay.Visibility = Visibility.Collapsed;
            memberBlk.Visibility = Visibility.Collapsed;
            showstat.Visibility = Visibility.Collapsed;
            showpts.Visibility = Visibility.Collapsed;
            pointsBlk.Visibility = Visibility.Collapsed;
            pointsTxt.Visibility = Visibility.Collapsed;
            redeemBtn.Visibility = Visibility.Collapsed;
            string searchname = guestTxt.Text;
            double redeemable = 0;
            
            string searchpassno = ppTxt.Text;
            if ((pointsTxt.Text) == "")
            {
                redeemable = 0;
            }
            else
            {
                redeemable = Convert.ToInt32(pointsTxt.Text);
            }
            
            msg.Text = "You have checked out! Thanks for choosing us!";

            foreach (Guest g in guestList)
            {
                if (searchname == g.Name || searchpassno == g.PpNumber)
                {
                    double newtotal = 0;
                    double newpoints = g.Membership.Points;
                    double maxreedemable = 0;
                    double scost = 0;
                    double dcost = 0;
                    if (g.Membership.Status == "Silver")
                    {
                        foreach(HotelRoom r in g.HotelStay.Roomlist)
                        {
                            if(r is StandardRoom)
                            {
                                scost += r.CalculateCharges() * (g.HotelStay.CheckOutDate - g.HotelStay.CheckInDate).Days;//Add to standard or deluxe
                                maxreedemable += (scost / 10); //Max redeemable for silvers
                            }
                            else
                            {
                                dcost += r.CalculateCharges() * (g.HotelStay.CheckOutDate - g.HotelStay.CheckInDate).Days;
                                //Do not add to maxredeemable if a silver books a deluxe room
                            }
                            
                                
                        }
                        if (redeemable >= maxreedemable)//if my redeemable points is lower than my scost, redeem normally
                        {
                            scost -= maxreedemable;
                            msg.Text = "Silvers can only offset Standard rooms";
                            newpoints -= maxreedemable;
                            redeemable = maxreedemable;
                        }
                        else
                        {
                            scost -= redeemable;
                            newpoints -= redeemable;
                            
                        }
                        newtotal = scost + dcost;// final total after deduction
                        g.Membership.EarnPoints(newtotal);//calc points to add to existing points
                        g.Membership.RedeemPoints(Convert.ToInt32(redeemable));//deduct redeemed points
                        




                    }
                    else if (g.Membership.Status == "Gold")
                    {
                        
                        newtotal = g.HotelStay.CalculateTotal();
                        if(redeemable > g.Membership.Points)
                        {
                            msg.Text = "Not enough points";
                        }
                        else
                        {
                            newtotal -= redeemable;
                            g.Membership.RedeemPoints(Convert.ToInt32(redeemable));
                            g.Membership.EarnPoints(newtotal);
                        }
                        
                    }
                    else if (g.Membership.Status == "Ordinary")
                    {
                        foreach (HotelRoom r in g.HotelStay.Roomlist)
                        {
                            availrooms.Add(r);
                            r.IsAvail = true;
                        }
                        newtotal = g.HotelStay.CalculateTotal();
                        g.Membership.EarnPoints(newtotal);
                    }
                    foreach (HotelRoom r in g.HotelStay.Roomlist)
                    {
                        if(r is DeluxeRoom)
                        {
                            DeluxeRoom d = (DeluxeRoom)r;
                            d.AdditionalBed = false;
                        }
                        else if(r is StandardRoom)
                        {
                            StandardRoom s = (StandardRoom)r;
                            s.RequireBreakfast = false;
                            s.RequireWifi = false;
                        }
                        r.IsAvail = true;
                        availrooms.Add(r);
                    }
                    g.HotelStay.Roomlist.Clear();
                    
                    
                }
            }

            availrmLv.ItemsSource = null;
            invoicedisplay.Text = "";
        }
        private void RemovermBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            HotelRoom remove = (HotelRoom)selectrmLv.SelectedItem;
            remove.IsAvail = true;
            HotelRoom removechosen = (HotelRoom)selectrmLv.SelectedItem;
            removechosen.IsAvail = true;
            if (selectrmLv.SelectedItem == null)
            {
                msg.Text = "Choose a room to remove";
            }
            else
            {
                foreach (HotelRoom r in selectedroom)
                {
                    if (r == removechosen)
                    {
                        if (removechosen is StandardRoom)
                        {
                            StandardRoom tstandard = (StandardRoom)removechosen;
                            tstandard.RequireBreakfast = false;
                            tstandard.RequireWifi = false;
                        }
                        else
                        {
                            DeluxeRoom tstandard = (DeluxeRoom)removechosen;
                            tstandard.AdditionalBed = false;
                        }
                        r.DailyRate = r.CalculateCharges();


                    }
                }
            }
            
            selectedroom.Remove(remove);
            availrooms.Add(remove);
            selectrmLv.ItemsSource = null;
            availrmLv.ItemsSource = null;
            selectrmLv.ItemsSource = selectedroom;
            availrmLv.ItemsSource = availrooms;

        }

        private void printinvoice()
        {
            string searchname = guestTxt.Text;
            string searchpp = ppTxt.Text;
            foreach (Guest g in guestList)
            {
                bool bfastcount = false;
                bool wificount = false;
                bool bedcount = false;
                if (g.Name == searchname || g.PpNumber == searchpp)
                {
                    double total = 0;
                    invoicedisplay.Text = "";
                    availrmLv.ItemsSource = g.HotelStay.Roomlist;
                    int datediff = (g.HotelStay.CheckOutDate - g.HotelStay.CheckInDate).Days;
                    foreach (HotelRoom r in g.HotelStay.Roomlist)
                    {
                        double subtotal = r.CalculateCharges() * datediff;
                        if (r is StandardRoom)
                        {
                            StandardRoom q = (StandardRoom)r;
                            if (q.RequireBreakfast)
                            {
                                bfastcount = true;
                            }
                            if (q.RequireWifi)
                            {
                                wificount = true;
                            }
                            total += subtotal;
                            invoicedisplay.Text += "Room Type:" + r.RoomType + "\nRoom No.:" + r.RoomNumber + "\nNo.of Nights:" + datediff + "\nWifi:" + wificount + "\nBreakfast:" + bfastcount + "\nSubtotal:" + subtotal + "\n\n";
                        }
                        else if (r is DeluxeRoom)
                        {
                            DeluxeRoom z = (DeluxeRoom)r;
                            if (z.AdditionalBed)
                            {
                                bedcount = true;
                            }
                            total += subtotal;
                            invoicedisplay.Text += "Room Type:" + r.RoomType + "\nRoom No.:" + r.RoomNumber + "\nNo.of Nights:" + datediff + "\nAdditional Beds:" + bedcount + "\nSubtotal:" + subtotal + "\n\n";
                        }
                    }

                }

            }
        }


        private void RedeemBtn_Click(object sender, RoutedEventArgs e)
        {
            msg.Text = "";
            if (pointsTxt.Text == "")
            {
                msg.Text = "You have not enter any points to redeem";
            }
            else
            {
                string searchname = guestTxt.Text;
                string searchpp = ppTxt.Text;


                foreach (Guest g in guestList)
                {
                    if (g.Name == searchname || g.PpNumber == searchpp)
                    {
                        int redeemable = Convert.ToInt32(pointsTxt.Text);
                        if (redeemable > g.Membership.Points)
                        {
                            msg.Text = "Not enough points to redeem";
                        }
                        else
                        {
                            //Check for redeem, also display output
                            double newtotal = 0;
                            double newpoints = g.Membership.Points;
                            double maxredeemable = 0;
                            double scost = 0;
                            double dcost = 0;
                            int deluxcount = 0;
                            int standcount = 0;
                            if (redeemable > g.HotelStay.CalculateTotal())
                            {
                                msg.Text = "The max points u can redeem is " + g.HotelStay.CalculateTotal();
                                break;
                            }
                            if (g.Membership.Status == "Ordinary")
                            {
                                msg.Text = "You are unable to redeem any points";
                            }
                            //Silver check
                            else if (g.Membership.Status == "Silver")
                            {
                                foreach (HotelRoom r in g.HotelStay.Roomlist)
                                {
                                    deluxcount++;
                                    if (r is DeluxeRoom && deluxcount <= 1)
                                    {
                                        msg.Text = "Silver member cannot redeem points for deluxeroom!";
                                    }
                                    else if (r is StandardRoom)
                                    {
                                        standcount++;
                                        scost += r.CalculateCharges() * (g.HotelStay.CheckOutDate - g.HotelStay.CheckInDate).Days;//Add to standard or deluxe
                                        maxredeemable += (scost / 10);//Max amount i can redeem for standard room 
                                        newtotal = g.HotelStay.CalculateTotal();
                                        newtotal -= redeemable;
                                        newpoints -= redeemable;
                                        showpts.Text = Convert.ToString(newpoints);
                                        showstat.Text = g.Membership.DetermineStat(newpoints);
                                        printinvoice();
                                        invoicedisplay.Text += "New Total: " + newtotal;
                                    }
                                    else if (standcount >= 1 && deluxcount >= 1)
                                    {
                                        if (redeemable > maxredeemable)
                                        {
                                            msg.Text = "Silvers can only offset Standard Rooms";
                                            redeemable = Convert.ToInt32(maxredeemable);
                                            break;
                                        }
                                        newtotal = g.HotelStay.CalculateTotal();
                                        newtotal -= redeemable;
                                        newpoints -= redeemable;
                                        showpts.Text = Convert.ToString(newpoints);
                                        showstat.Text = g.Membership.DetermineStat(newpoints);
                                        printinvoice();
                                        invoicedisplay.Text += "New Total: " + newtotal+"\n\n\n";
                                    }

                                }
                            }
                            else
                            {
                                newtotal = g.HotelStay.CalculateTotal();

                                newtotal -= redeemable;
                                newpoints -= redeemable;
                                showpts.Text = Convert.ToString(newpoints);
                                showstat.Text = g.Membership.DetermineStat(newpoints);
                                printinvoice();
                                invoicedisplay.Text += "New Total: " + newtotal+"\n\n\n";
                            }
                        }
                    }
                }
            }

        }

                    //{
                    //    foreach (HotelRoom r in g.HotelStay.Roomlist)//Foreach room in guest
                    //    {
                    //        if (r is StandardRoom)
                    //        {
                    //            scost += r.CalculateCharges() * (g.HotelStay.CheckOutDate - g.HotelStay.CheckInDate).Days;//Add to standard or deluxe
                    //            maxredeemable += (scost / 10);//Max amount i can redeem for standard room 
                    //        }
                    //        else if (r is DeluxeRoom)
                    //        {
                    //            dcost += r.CalculateCharges() * (g.HotelStay.CheckOutDate - g.HotelStay.CheckInDate).Days;
                    //        }
                    //    }
                    //    if (redeemable < maxredeemable)
                    //    {
                    //        scost -= maxredeemable;
                    //        msg.Text = "Silvers can only offset Standard Rooms";
                    //        newpoints -= maxredeemable;
                    //        redeemable = Convert.ToInt32(maxredeemable);
                    //    }
                    //    else
                    //    {
                    //        scost -= 0;
                    //        newpoints -= 0;
                    //    }

                    //    newtotal = scost + dcost;

                    //    showpts.Text = Convert.ToString(newpoints);
                    //    showstat.Text = Convert.ToString(g.Membership.DetermineStat(newpoints));
                    //    printinvoice();
                    //    invoicedisplay.Text += "New Total: " + newtotal;

                    //}
                    //else
                    //{
                    //    newtotal = g.HotelStay.CalculateTotal();

                    //    newtotal -= redeemable;
                    //    newpoints -= redeemable;
                    //    showpts.Text = Convert.ToString(newpoints);
                    //    showstat.Text = g.Membership.DetermineStat(newpoints);
                    //    printinvoice();
                    //    invoicedisplay.Text += "New Total: " + newtotal;
                    //}



                    //    }
                    //}
                        
                    //if (g.Membership.Status == "Ordinary")
                    //{
                    //    msg.Text = "You can only redeem points if u are silver or gold!";
                    //}
        //        }

        //    }
        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            leaderboard.Text = "";
            msg.Text = "";
            guestList.Sort();
            guestList.Reverse();
            selectrmLv.Visibility = Visibility.Collapsed;
            leaderboard.Visibility = Visibility.Visible;
            foreach(Guest g in guestList)
            {
                leaderboard.Text += "\n"+g.Name +"\t"+ g.Membership.Points+"\n";
            }
        }
    }
}

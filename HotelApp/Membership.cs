﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelApp
{
    class Membership
    {
        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private int points;

        public int Points
        {
            get { return points; }
            set { points = value; }
        }

        public Membership() { }

        public Membership(string s, int p)
        {
            this.status = s;
            this.points = p;
        }

        public void EarnPoints(double c)
        {
            Points += Convert.ToInt32((c / 10));

        }
        public bool RedeemPoints(int p)
        {
            if (p > Points)
            {
                return false;
            }
            else
            {
                Points -= p;
                return true;
            }
        }
        public string DetermineStat(double p)
        {
            if (Points >= 200)
            {
                Status = "Gold";
            }
            else if(Points <200 && Status == "Gold")
            {
                Status = "Gold";
            }
            else if((Points >= 100) && Status != "Gold")
            {
                Status = "Silver";
            }
            else if(Points < 100 && Status == "Silver")
            {
                Status = "Silver";
            }
            else
            {
                Status = "Ordinary";
            }
            return Status;
        }
        
    }   
}